import 'package:flutter/material.dart';

class ConstrainedBoxs extends StatelessWidget {
  const ConstrainedBoxs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Center(
            child: Text(
              'ประวัตินิสิต',
              style: TextStyle(
                height: 5,
                  fontSize: 23.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Image.network(
            "https://pbs.twimg.com/profile_images/975664337032376321/9hrwacvq_400x400.jpg",
            height: 100,
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText1,
              style: TextStyle(fontSize: 14.0,
                  fontWeight: FontWeight.w600,
              height: 2
              ),
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText3,
              style: TextStyle(
                  fontSize: 23.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.red),
            ),
          ),
          Image.network(
              "https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/325429589_2534626980009002_9025078851024509645_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeFZg43dUuK6maBfnedyCasa7DD0CgvAarzsMPQKC8BqvAZmMPS_1x1vaQDNCvE_QrKkcxhxFhP-03vl9Nnyh7Ww&_nc_ohc=IcWgzG-Px-gAX9cRXVU&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfCIleHEwVmsuMAHYYrBkTnyOnHegW8Q65m2DOHI-_-FlQ&oe=63D71C9B"),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText2,
              style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
            ),
          ),
        ],
      ),
    );
  }
}

String _longText1 =
    'รหัสนิสิต : 63160193 \nชื่อ-นามสกุล : นางสาวญาณิกา คงภักดี \nคณะ : คณะวิทยาการสารสนเทศ \nสาขา : วิทยาการคอมพิวเตอนฃร์ \nหลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: - \nสถานภาพ: กำลังศึกษา \nอ. ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน';

String _longText2 =
    '**ค่าไฟฟ้าห้องพักนิสิต กองกิจการนิสิต ภาคปลาย ปีการศึกษา 2565 \nปร ะจำเดือน ธันวาคม พ.ศ. 2565 \n >> ตรวจสอบรายชื่อนิสิตค้างชำระค่าไฟฟ้า << \n- นิสิตสามารถตรวจสอบรายชื่อโดยแสกน qrcode ด้านบน หรือทาง facebook : สำนักงานหอพักนิสิต มหาวิทยาลัยบูรพา ';

String _longText3 = '* ประกาศจากสำนักงานหอพักนิสิต';
