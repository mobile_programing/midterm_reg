import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:midterm/ConstrainedBoxs.dart';

import 'NavBar.dart';

void main() {
  runApp(const ColumnExample());
}

class ColumnExample extends StatelessWidget {
  const ColumnExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'REG Burapha University',
        theme: ThemeData(
          // primarySwatch: Colors.grey,

        ),
        home: Scaffold(
          resizeToAvoidBottomInset: false,
          drawer: NavBar(),
          backgroundColor: Colors.white,
          appBar: AppBar(
            // title: const Text('REG Burapha University'),
            title: const Text('ข้อมูลส่วนบุคคล'),
            backgroundColor: Colors.blue,
          ),
          body:SingleChildScrollView(
            child:Column(
              children: const <Widget>[
              head(),
              ImageExample(),
                History(),
              Bodycenter1(),
                Personal(),
                Bodycenter2(),
            ]
            ),

          ),
        ),
      ),
    );
  }
}

class ImageExample extends StatelessWidget {
  const ImageExample({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30.0),
      child: Column(

        children: [
          Image.network("https://pbs.twimg.com/profile_images/975664337032376321/9hrwacvq_400x400.jpg",
            height: 100,

          )
        ],
      ),
    );
  }
}

class head extends StatelessWidget {
  const head({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 100,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(50),

            ),
            color: Color(0xffb3e5fc),
          ),
          child: Stack(
            children: [
              Positioned(
                  top:30,
                  left: 0,
                  child: Container(
                    height: 50,
                    width: 300,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50),
                        bottomRight: Radius.circular(50),
                      )
                    ),
              )
              ),
              Positioned(
                  top: 39,
                  left: 20,
                  child: Text("ประวัตินิสิต", style: TextStyle(
                fontSize: 24,
                color: Colors.blue[800],
                      fontWeight: FontWeight.bold

              ),
              ),
              ),
            ],
          ),
        ),
    ]
    );
  }
}


class History extends StatelessWidget {
  const History({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          Container(
            height: 300,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(40),
                bottomRight: Radius.circular(40),
                topLeft: Radius.circular(40),
                bottomLeft: Radius.circular(40),

              ),
              color: Color(0xffb3e5fc),
            ),
            child: Stack(
              children: [
                Positioned(
                    top:15,
                    left: 10,
                    child: Container(
                      height: 270,
                      width: 370,
                      decoration: BoxDecoration(
                        
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(40),
                            bottomRight: Radius.circular(40),
                            topLeft: Radius.circular(40),
                            bottomLeft: Radius.circular(40),
                          )
                      ),
                    )
                ),
                Positioned(
                  top: 28,
                  left: 35,
                  child:
                 Container(
                  height: 250,
                  width: 350,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("ระเบียนประวัติ", style: TextStyle(
                          fontSize: 30,
                          color: Colors.blue[800],
                          fontWeight: FontWeight.bold
                      ),
                      ),
                      Text("รหัสประจำตัว :      63160193 ", style: TextStyle(
                        fontSize: 18,
                        color: Colors.blue[800],
                       ),
                        ),
                      Text("ชื่อ :            นางสาว ญาณิกา  คงภักดี",style: TextStyle(
                      fontSize: 18,
                      color: Colors.blue[800],
                      ),
                      ),
                      Text("คณะ :         วิทยาการสารสนเทศ",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                      ),
                      ),
                      Text("สาขา :        วิทยาการคอมพิวเตอร์",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                      ),
                      ),
                      Text("วิทยาเขต : บางแสน",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                      ),
                      ),
                      Text("ระดับการศึกษา :   ปริญญาตรี",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                      ),
                      ),
                      Text("ปีที่เข้าศึกษา :        วันที่ 21/5/2563 ",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                      ),
                      ),
                      Text("จบการศึกษาจาก :    เหนือคลองประชาบำรุง ",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                      ),
                      ),
                    ],
                  ),
    ),
                ),
              ],
            ),
          ),
        ]
    );
  }
}


class Bodycenter1 extends StatelessWidget {
  const Bodycenter1({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 131,
      left: 20,
      child: Column(
        children: [
          Container(
            height: 20,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(40),
                bottomRight: Radius.circular(40),
                topLeft: Radius.circular(40),
                bottomLeft: Radius.circular(40),

              ),
              color: Colors.white,
            ),
          ),
        ]
    )
    );
  }
}


class Personal extends StatelessWidget {
  const Personal({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          Container(
            height: 250,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(40),
                bottomRight: Radius.circular(40),
                topLeft: Radius.circular(40),
                bottomLeft: Radius.circular(40),

              ),
              color: Color(0xffb3e5fc),
            ),
            child: Stack(
              children: [
                Positioned(
                    top:15,
                    left: 10,
                    child: Container(
                      height: 220,
                      width: 370,
                      decoration: BoxDecoration(

                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(40),
                            bottomRight: Radius.circular(40),
                            topLeft: Radius.circular(40),
                            bottomLeft: Radius.circular(40),
                          )
                      ),
                    )
                ),
                Positioned(
                  top: 28,
                  left: 35,
                  child:
                  Container(
                    height: 195,
                    width: 350,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("ข้อมูลส่วนบุคคล", style: TextStyle(
                            fontSize: 30,
                            color: Colors.blue[800],
                            fontWeight: FontWeight.bold
                        ),
                        ),
                        Text("สัญชาติ :      ไทย ", style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                        ),
                        ),
                        Text("ศาสนา :       พุทธ",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                        ),
                        ),
                        Text("ชื่อบิดา :        -",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                        ),
                        ),
                        Text("ชื่อมารดา :       -",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                        ),
                        ),
                        Text("ที่อยู่ : -",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                        ),
                        ),
                        Text("ที่อยู่ ( ผู้ปกครอง ) :   -",style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                        ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]
    );
  }
}


class Bodycenter2 extends StatelessWidget {
  const Bodycenter2({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 130,
        left: 20,
        child: Column(
            children: [
              Container(
                height: 20,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(40),
                    bottomRight: Radius.circular(40),
                    topLeft: Radius.circular(40),
                    bottomLeft: Radius.circular(40),

                  ),
                  color: Colors.white,
                ),
              ),
            ]
        )
    );
  }
}


