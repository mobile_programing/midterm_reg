import 'package:flutter/material.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Remove padding
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('ญาณิกา คงภักดี'),
            accountEmail: Text('63160193@go.buu.ac.th'),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network(
                  "https://pbs.twimg.com/profile_images/975664337032376321/9hrwacvq_400x400.jpg",
                  fit: BoxFit.cover,
                  width: 100,
                  height: 100,
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.grey.shade800,
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                "https://cdn.pixabay.com/photo/2021/11/14/20/08/background-6795624_960_720.jpg",
                      )),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home,color: Colors.blue,),
            title: Text('เมนูหลัก'),
            onTap: () => null,

          ),
          ListTile(
            leading: Icon(Icons.notifications,color: Colors.blue,),
            title: Text('ลงทะเบียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.calendar_today,color: Colors.blue,),
            title: Text('ตารางเรียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.person,color: Colors.blue,),
            title: Text('ประวัตินิสิต'),
            onTap: () => null,
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.monetization_on,color: Colors.blue,),
            title: Text('ค่าใช้จ่าย'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.description,color: Colors.blue,),
            title: Text('ยื่นคำร้อง'),
            onTap: () => null,
          ),
          Divider(),
          ListTile(
            title: Text('เกี่ยวกับ'),
            leading: Icon(Icons.settings,color: Colors.blue,),
            onTap: () => null,
          ),
          ListTile(
            title: Text('ออกจากระบบ'),
            leading: Icon(Icons.exit_to_app,color: Colors.blue,),
            onTap: () => null,
          ),
        ],
      ),
    );
  }
}
